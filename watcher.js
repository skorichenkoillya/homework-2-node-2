const fs = require('fs');
const path = require('path');


fs.watch(path.join(__dirname, './dist'),(eventType, fileName) => {
    const fileThatWasChanged = path.join(__dirname, './dist', fileName);
    const fileStatus = fs.existsSync(fileThatWasChanged);
    if (! fileStatus) {
        return console.log('File was deleted');
    }
    const content = fs.readFileSync(fileThatWasChanged).toString();
    if (content) {
        const parsedData = JSON.parse(content);
        fs.unlinkSync(fileThatWasChanged);
        switch (parsedData.type){
            case 'gas':
                fs.appendFile(
                    path.join(__dirname, './audit/gas.csv'),
                    `\n${parsedData.date},${parsedData.value},${parsedData.name}`,
                    () => console.log('written')
                );
                break;
            case 'water':
                fs.appendFile(
                    path.join(__dirname, './audit/water.csv'),
                    `\n${parsedData.date},${parsedData.value},${parsedData.name}`,
                    () => console.log('written')
                );
                break;
        }
    }

})