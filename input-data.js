const readline = require("readline");
const fs = require("fs");
const path = require("path")

const rl = readline.createInterface({
    output: process.stdout,
    input: process.stdin
})

function askAsync(question, validate) {
    return new Promise((resolve, reject) => {
        rl.question(question, (data) => {
            validate(data, reject);
            console.log(data, 'received');
            resolve(data);
        });
    })
}

async function ask() {
    const data = {
        type: '',
        value: 0,
        date: new Date(),
        name: ''
    }
    const type = await askAsync('Water or gas: ', (data, reject) => {
        required(data, reject);
        if (data.toLowerCase() !== 'water' && data.toLowerCase() !== 'gas') {
            return reject(new Error('Incorrect argument'));
        }
    })
    data.type = type.toLowerCase();
    data.value = await askAsync('Input value: ', (data, reject) => {
        required(data, reject);
        if (data.length < 6 || !data.match(/^\d+$/)) {
            return reject(new Error('Incorrect argument'));
        }
    });
    data.date = await askAsync('Input date: ', (data, reject) => {
        required(data, reject);
        if (!data.match(/(\d{2}-){2}\d{4}/)) {
            return reject(new Error('Incorrect argument'));
        }
    });
    data.name = await askAsync('Input name: ', (data, reject) => {
        required(data, reject);
        if (!data.match(/^[A-Za-z]+$/)) {
            return reject(new Error('Incorrect argument'));
        }
    });
    fs.writeFileSync(
        path.join(__dirname, './dist/' + new Date() + 'info.json'),
        JSON.stringify(data)
    )
    rl.close();
}

ask();

function required(data, reject) {
    if (!data) {
        return reject(new Error('Data required'));
    }
    return true;
}

