const readline = require("readline");
const fs = require("fs");
const path = require("path");


const createRl = (filePath) => readline.createInterface({
    input: fs.createReadStream(filePath)
})

async function parse(filePath) {
    const rl = createRl(filePath);
    const records = [];
    for await (const line of rl) {
        if (line)  {
            const {'0': date, '1': value, '2': name} = {...line.split(',')}
            records.push({date, value, name})
        }
    }
    return records.reduce((accumulator, record) => {
        const existUser = accumulator.find(users => users.name === record.name);
        if (!existUser) {
            return [...accumulator, {
                name: record.name,
                records: [
                    {...record}
                ]
            }]
        }
        existUser.records.push({...record});
        return [
            {...existUser}
        ]
    }, [])
}

function output(usersData, key) {
    console.log(`_\n${key}\n|`)
    usersData.map((userData, index) => {
        console.log(`-----${index}---User: ${userData.name}`);
        userData.records.map((record) => {
            console.log(`${record.date} - ${record.value}`)
        });
    })
}


function main() {
    const filesNames = [
        'gas',
        'water'
    ]
    filesNames.map((fileName) => {
        const filePath = path.join(__dirname, './audit', `${fileName}.csv`);
        fs.exists(filePath, async (exists) => {
            if (exists) {
                const usersData = await parse(filePath);
                output(usersData, fileName.toUpperCase());
            }
        })
    })
}

main();